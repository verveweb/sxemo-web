# Добро пожаловать в Sxemo.

Это максимально простой редатор схем, мы постарались соблюсти принцип KISS во всем. Никаких лишних функций, только самые необходимые, чтобы быстро набрасывать структуру чего-нибудь, план или взаимосвязи.

## Как это работает.
У вас есть списом схем, в каждой схеме есть блоки, которые можно добавлять, удалять и соединять между собой. У каждого блока есть заголовок и описание. Так же каждый блок может содержать внутри себя еще одну схему.

## Управление.
Нам кажется, что управление интуитивно понятно, но все же. Некоторые операции можно распротронить на потомков с нажатой клавишей Shift.

**NB** Кнопки управления блоком и в списке схем появляются при наведении курсора на блок. 

**P.S.** Тестировалось только в Chrome, работоспособность в других браузерах не проверена.

## Схемы 
Функция | Действие
-----------|-----------
Добавить схему | Кнопка "+" в заголовке
Удалить схему | В списке схем справа кнопка удалить или кнопка удаления в заголовке для внутренних схем блоков
Переименовать схему | Двойной клик по заголовку

## Блоки 
Функция | Действие 
-----------|------------
Создать блок | Двойной клик по рабочей области или подключение к пустому месту на рабочей области
Соединить блоки | Перетяните выход одного блока на другой блок
Соединение нескольких блоков | Начните тянуть выход через блоки с зажатым Shift
Разъединить | Двойной клик по соединению или входу / выходу
Переместить блок | Потяните за заголовок
Переместить блок со всеми потомками  | Потяните за заголовок с зажатым Shift
Переименовать блок | Двойной клик по заголовку

### Кнопки управления
Положение | Значок  | Функция
--------------|-----------|-----------
Вверху | Солнце | Подсветить все прямые соединения блока
Вверху | Схема | Перейти на внутреннюю схему блока (При наличии схемы у блока появляется значок)
Вверху | Замок | Заблокировать/разблокировать блок - невозможно перемещение, удаление и редактирование данных 
| | Заблокировать/разблокировать блок и всех потомков - зажмите Shift 
Внизу | Корзина | Удаление блока (Невозможно отметить, удаляются все схемы и блоки внутри)