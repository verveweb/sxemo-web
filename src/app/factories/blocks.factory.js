'use strict'

export default function ( defaultBlock, Tools )
{
	const blocks = this

	blocks.create = function ( parent )
		{
			var block = new defaultBlock()

			block.position.left = window.innerWidth / 10
			block.position.top = window.innerHeight / 3

			block.uid = 'B-' + Tools.guid()
			block._parent = parent

			return block
		}

	return blocks
}