'use strict'

export default function ( defaultSchema, BlocksFactory, Tools )
{
	const schemas = this

	schemas.create = function ()
		{
			var sch = new defaultSchema()

			sch.uid = 'S-' + Tools.guid()
			sch.blocks.push(BlocksFactory.create(sch))
			
			return sch
		}

	return schemas
}