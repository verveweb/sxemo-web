'use strict'

export default function ( $scope )
{
	const dlg = this

	dlg.no = $scope.closeThisDialog

	dlg.yes = function()
		{
			$scope.closeThisDialog(true)
		}
}