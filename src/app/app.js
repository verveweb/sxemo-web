'use strict'

/* ====== CONFIG =========== */

import Routing from './routing.js'


/* ======= DEPENDENCIES ==== */

import angular      from 'angular'
import ngResource   from 'angular-resource'
import ngSanitize   from 'angular-sanitize'
import ngRoute      from 'angular-route'
import ngDialog     from 'ng-dialog'


/* ====== COMPONENTS ======= */

import sxemoBlock       from './directives/sxemo-block/sxemo-block.directive.js'
import sxemoSchema      from './directives/sxemo-schema/sxemo-schema.directive.js'
import sxemoConnector   from './directives/sxemo-connector/sxemo-connector.directive.js'
import sxemoEdit        from './directives/sxemo-edit/sxemo-edit.directive.js'
import sxemoArea        from './directives/sxemo-area/sxemo-area.directive.js'
import sxemoSelect      from './directives/sxemo-select/sxemo-select.directive.js'
import sxemoStatus      from './directives/sxemo-status/sxemo-status.directive.js'
import sxemoColors      from './directives/sxemo-colors/sxemo-colors.directive.js'


/* ====== RESOURCES ===== */

import API      from './resources/api.resource.js'


/* ====== SERVICES ========= */

import Dialogs          from './services/dialogs.service.js'
import Tools            from './services/tools.service.js'

import Dragger          from './services/dragger.service.js'
import Resizer          from './services/resizer.service.js'
import Line             from './services/line.service.js'
import Workspace        from './services/workspace.service.js'
import Status           from './services/status.service.js'
import DB               from './services/db.service.js'
import Settings         from './services/settings.service.js'

import Schemas          from './services/schemas.service.js'
import Blocks           from './services/blocks.service.js'


/* ===== FACTORIES ========= */

import SchemasFactory   from './factories/schemas.factory.js'
import BlocksFactory    from './factories/blocks.factory.js'


/* ====== VALUES ====== */

import defaultBlock     from './values/default-block.value.js'
import defaultSchema    from './values/default-schema.value.js'


/* ======= STYLES =========== */

import '../style/reset.css'
import '../style/style.css'
import '../style/font-awesome.min.css'


/* ======= APP INIT ======== */

const 
    MODULE_NAME = 'awpApp',
    APP_MODULES = [ 'ngRoute', 'ngResource', 'ngSanitize', 'ngDialog' ]

const app = angular.module(MODULE_NAME, APP_MODULES)
    .constant('apiBaseUrl', API_URL)

    //Values
    .value('defaultBlock', defaultBlock)
    .value('defaultSchema', defaultSchema)

    //Directives
    .directive('sxemoBlock', sxemoBlock)
    .directive('sxemoSchema', sxemoSchema)
    .directive('sxemoConnector', sxemoConnector)
    .directive('sxemoEdit', sxemoEdit)
    .directive('sxemoArea', sxemoArea)
    .directive('sxemoSelect', sxemoSelect)
    .directive('sxemoStatus', sxemoStatus)
    .directive('sxemoColors', sxemoColors)

    //Resources & services
    .factory('API', API)
    .factory('Dialogs', Dialogs)
    .factory('Dragger', Dragger)
    .factory('Resizer', Resizer)

    .factory('Schemas', Schemas)
    .factory('Blocks', Blocks)
    .factory('Line', Line)

    .service('Tools', Tools)
    .service('Workspace', Workspace)
    .service('Status', Status)
    .service('DB', DB)
    .service('Settings', Settings)

    //Factories
    .factory('SchemasFactory', SchemasFactory)
    .factory('BlocksFactory', BlocksFactory)

//Apply config
Routing(app)

export default MODULE_NAME