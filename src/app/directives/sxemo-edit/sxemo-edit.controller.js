'use strict'

export default function ( $scope, $element, $timeout )
{
	const edit = this,
		$input = $element[0].querySelector('input')

	edit.state = false

	edit.start = function ()
		{
			if ($scope.readonly) return

			edit.state = true
		
			$timeout(function(){
				$input.focus()
				$input.setSelectionRange(0, $input.value.length)
			}, 200)
		}

	edit.enter = function ( e )
		{
			if (e.keyCode !== 13) return
			edit.save()
		}

	edit.save = function ()
		{
			edit.state = false
			;($scope.auto !== undefined) && ($scope.auto = false)
			;($scope.onChange) && ($scope.onChange($scope.model))
		}

	;($scope.auto) && (edit.start())
}