'use strict'

import editCtrl from './sxemo-edit.controller.js'
import editTmpl from './sxemo-edit.template.html'

import '../../../style/components/sxemo-edit.css'

export default function()
{
	return {
		replace: true,
		restrict: 'E',
		scope:
			{
				model: '=model',
				auto: '=',
				readonly: '=',
				onChange: '='
			},
		template: editTmpl,
		controller: editCtrl,
		controllerAs: 'edit'
	}
}