'use strict'

import colorsCtrl from './sxemo-colors.controller.js'
import colorsTmpl from './sxemo-colors.template.html'

import '../../../style/components/sxemo-colors.css'

export default function()
{
	return {
		replace: true,
		restrict: 'E',
		scope:
			{
				model: '=',
				onSelect: '='
			},
		template: colorsTmpl,
		controller: colorsCtrl,
		controllerAs: 'colors'
	}
}