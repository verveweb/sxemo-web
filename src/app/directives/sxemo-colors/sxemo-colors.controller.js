'use strict'

export default function ( $scope, Tools )
{
	const colors = this

	colors.options = 
		[
			'none', 'violet', 'blue', 'cyan', 'green', 'yellow', 'orange', 'red', 'white'
		]

	colors.model = $scope.model
	colors.open = false

	$scope.$watch('model', function(){
		colors.model = $scope.model
	})

	colors.select = function ( item )
		{
			;($scope.onSelect) && ($scope.onSelect(item))
			$scope.model = item
			colors.model = item
			colors.open = false
		}

	angular.element(document).bind('click', function ( e ) {
		if (!Tools.has.parent(e.target, 'sxemo-colors'))
		{
			$scope.$apply(function(){
				colors.open = false
			})
		}
	})
}