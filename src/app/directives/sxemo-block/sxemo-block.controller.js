'use strict'

export default function ( $scope, $element, Dragger, Resizer, Blocks, Workspace, SchemasFactory, Tools, DB, Dialogs )
{
	const block = this,
		$footer = $element[0].querySelector('.sxemo-block-resize')

	block._element = $element[0]
	block.model = $scope.model
	block.model._element = block
	block.autoStart = (block.model === Workspace.get.block())
	block.connectors = []

	Workspace.watch('block', function (){
		block.autoStart = (Workspace.get.block() === block.model)
	})

	block.move = ( x, y, e, preventApply, prevents ) =>
		{
			prevents = prevents || []

			;(preventApply)
				? move(x,y,e, prevents)
				: $scope.$apply(() => { move(x,y,e, prevents) })
		}

	function move ( x, y, e, prevents )
		{
			var diffX = x - block.model.position.left,
				diffY = y - block.model.position.top

			if (prevents.indexOf(block.model.uid) > -1) return

			block.model.position.left = x
			block.model.position.top  = y

			if (e && e.shiftKey)
			{
				block.highlight(true, true, false)
				prevents.push(block.model.uid)

				block.model.returns.map(function(uid){
					var b = Workspace.findBlock(uid),
						ox = Math.max(0, b.position.left + diffX),
						oy = Math.max(0, b.position.top + diffY)

					b._element.move(ox, oy, e, true, prevents)
				})
			}
			else
			{
				cascadeDisHL(block, [block.model.uid])
			}

			Blocks.save(block.model)
		}

	function cascadeDisHL ( block, prevents )
		{
			if (!block) return

			block.highlight(false, true, false)

			block.model.returns.map(function(uid){
				if (prevents.indexOf(uid) < 0) cascadeDisHL(Workspace.findBlock(uid)._element, prevents.concat(uid))
			})
		}


	block.resize = function ( x, y )
		{
			$scope.$apply(function(){
				block.model.height = Math.max(Math.max(block.model.inserts.length, block.model.returns.length) * 15 + 65, y)
				Blocks.save(block.model)	
			})
		}

	block.unset = function ( con, rev )
		{
			;(rev)
				? Blocks.disconnect(block.model, Workspace.findBlock(con))
				: Blocks.disconnect(Workspace.findBlock(con), block.model)
		}

	block.remove = function ()
		{
			if (block.model.schema !== null)
			{
				Dialogs.removeSchema('Блок содержит схему', function ( answer ){
					if (answer === true) Blocks.remove(block.model)
				})
			}
			else
			{
				Blocks.remove(block.model)
			}
		}

	block.lock = function ( e, bl, state, prevents )
		{
			bl = bl || block.model
			prevents = prevents || []

			if (prevents.indexOf(bl.uid) > -1) return

			;(state === undefined) 
				? (bl.locked = !bl.locked) && (state = bl.locked)
				: bl.locked = state

			Blocks.save(bl)

			if (e.shiftKey)
			{
				prevents.push(bl.uid)

				bl.returns.map(function ( uid ) {
					block.lock(e, Workspace.findBlock(uid), state, prevents)
				})
			}
		}

	block.setColor = function ( color )
		{
			block.model.color = color
			block.save()
		}

	block.save = function ()
		{
			Blocks.save(block.model)
		}

	block.openSchema = function ()
		{
			if (block.model._schema === null)
			{
				block.model._schema = SchemasFactory.create()
				block.model._schema.title = block.model.title + ' Schema'
				
				block.model.schema = block.model._schema.uid				

				DB.save.schema(block.model._schema)
				block.save()
			}

			block.model._schema._parent = block.model._parent
			block.model._schema.parent = block.model._parent.uid
			block.model._schema._block = block.model.uid

			Workspace.set.schema(block.model._schema)
		}

	block.highlight = function ( state, returns, inserts )
		{
			block._highlight = state

			if (inserts)
			{
				block.model.inserts.map(function ( origin ) {
					let conn = Workspace.findReturn(block.model.uid, origin),
						bl = document.getElementById(origin)

					;(state) 
						? (conn.className += ' highlight', bl.className += ' hl')
						: (conn.className = conn.className.replace(/ highlight/g, ''), bl.className = bl.className.replace(/ hl/g, ''))
				})
			}

			if (returns)
			{
				block.model.returns.map(function ( target ) {
					let bl = document.getElementById(target)
					if (!bl) return 
					;(state) ? bl.className += ' hl' : bl.className = bl.className.replace(/ hl/g, '')
				})
			}
		}
		
	block.drag = new Dragger(block.move)
	block.resizer = new Resizer(block.resize)

	angular.element(document).bind('mouseup', function ( e ){
		if (block._element.contains(e.target))
		{
			cascadeDisHL(block, [])
			;(!Tools.has.parent(e.target, 'sxemo-block-resize')) && (block.resize(0,block.model.height))
		}
	})
}