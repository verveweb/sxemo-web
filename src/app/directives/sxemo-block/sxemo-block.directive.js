'use strict'

import blockCtrl from './sxemo-block.controller.js'
import blockTmpl from './sxemo-block.template.html'

import '../../../style/components/sxemo-block.css'

export default function()
{
	return {
		replace: true,
		restrict: 'E',
		scope:
			{
				model: '=model'
			},
		template: blockTmpl,
		controller: blockCtrl,
		controllerAs: 'block'
	}
}