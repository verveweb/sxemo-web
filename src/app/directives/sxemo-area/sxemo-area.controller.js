'use strict'

export default function ( $scope, $element, $timeout )
{
	const area = this,
		$input = $element[0].querySelector('textarea')

	area.state = false

	area.start = function ()
		{
			if ($scope.readonly) return
				
			area.state = true
		
			$timeout(function(){
				$input.focus()
				$input.setSelectionRange($input.value.length, $input.value.length)
			}, 200)
		}

	area.save = function ()
		{
			area.state = false
			;($scope.auto !== undefined) && ($scope.auto = false)
			;($scope.onChange) && ($scope.onChange($scope.model))
		}

	;($scope.auto) && (area.start())
}