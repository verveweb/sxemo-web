'use strict'

import areaCtrl from './sxemo-area.controller.js'
import areaTmpl from './sxemo-area.template.html'

import '../../../style/components/sxemo-area.css'

export default function()
{
	return {
		replace: true,
		restrict: 'E',
		scope:
			{
				model: '=model',
				readonly: '=',
				onChange: '='
			},
		template: areaTmpl,
		controller: areaCtrl,
		controllerAs: 'area'
	}
}