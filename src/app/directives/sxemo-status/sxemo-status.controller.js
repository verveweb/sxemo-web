'use strict'

export default function ( $scope, Status, $timeout, $element )
{
	const status = this,
		_timeouts = []

	status.text = ''
	status.show = false

	status.add = function ( text )
		{
			_timeouts.map(function ( t ) { $timeout.cancel(t) })

			status.show = false

			_timeouts[0] = $timeout(function(){
				status.text = text
				status.show = true

				_timeouts[1] = $timeout(function(){
					status.show = false
				}, 3000)
			}, 200)
		}

	Status.watch(status.add)
}