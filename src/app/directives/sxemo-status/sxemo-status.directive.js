'use strict'

import statusCtrl from './sxemo-status.controller.js'
import statusTmpl from './sxemo-status.template.html'

import '../../../style/components/sxemo-status.css'

export default function()
{
	return {
		replace: true,
		restrict: 'E',
		template: statusTmpl,
		controller: statusCtrl,
		controllerAs: 'status'
	}
}