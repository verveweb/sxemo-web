'use strict'

import schemaCtrl from './sxemo-schema.controller.js'
import schemaTmpl from './sxemo-schema.template.html'

import '../../../style/components/sxemo-schema.css'

export default function()
{
	return {
		replace: true,
		restrict: 'E',
		scope:
			{
				model: '=model'
			},
		template: schemaTmpl,
		controller: schemaCtrl,
		controllerAs: 'schema'
	}
}