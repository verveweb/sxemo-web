'use strict'

export default function ( $scope, $element, Schemas, Blocks, BlocksFactory, Workspace, Tools, Status, Dialogs )
{
	//Vars
	const schema = this,
		$workspace = $element[0].querySelector('.sxemo-schema-workspace')

	//Props
	schema.model = $scope.model
	schema.schemas = Schemas.list
	schema.catched =
		{
			x: null,
			y: null
		}

	//Watchers
	$scope.$watch('model', function(){
		schema.model = $scope.model

		if (
			schema.model.parent !== null &&
			schema.model.parent !== undefined &&
			(schema.model._parent === null || schema.model._parent === undefined)
		){
			schema.model._parent = Schemas.find(schema.model.parent, schema.model._block)
		}
	})

	//Methods
	schema.settings = () => Dialogs.settings()
	schema.help = () => Dialogs.help()
	schema.select = sch => Workspace.set.schema(sch)
	schema.return = () => Workspace.set.schema(schema.model._parent)
	schema.removeFromBlock = sch => Dialogs.removeSchema('', (answer) => {(answer === true) && (removeFromBlock())})

	schema.save = sch =>
		{
			;(typeof sch === 'string') && (sch = schema.model)
			Schemas.saveSchema(sch)
		}


	schema.remove = sch =>
		{
			sch._removed = true
			;(schema.model === sch) && (Workspace.set.schema(Schemas.get()))
			Schemas.save(Schemas.list)
		}

	schema.restore = sch =>
		{
			sch._removed = false
			Schemas.save(Schemas.list)
		}

	schema.add = () =>
		{
			Workspace.set.schema(Schemas.add())
			Schemas.save(Schemas.list)
		}

	schema.export = () =>
		{
			Schemas.export()
		}

	schema.block =
		{
			add:  ( e, connect ) =>
				{
					if (e.target !== $workspace) return

					const block = schema.block.create(e)

					schema.model.blocks.push(block)
					;(connect) && (Blocks.connect(Workspace.get.block(), block))

					Schemas.saveSchema(schema.model)
				},
			create: e =>
				{
					var block = BlocksFactory.create(schema.model)

					block.title += ' ' + (schema.model.blocks.length + 1)

					block.position.left = Math.max(0, e.pageX + $workspace.scrollLeft)
					block.position.top = Math.max(0, e.pageY - 110 + $workspace.scrollTop)

					return block
				}
		}

	schema.dropHandle = e =>
		{
			if (!Workspace.get.block())
				{
					schema.drag.end(e)
					return
				}

			if (e.target == $workspace)
				{
					schema.block.add(e, true)
				}
			else if (Tools.has.parent(e.target, 'sxemo-block'))
				{
					Blocks.connect(Workspace.get.block(), Workspace.findBlock(Tools.get.parent(e.target, 'sxemo-block').id))
				}
			else
				{
					console.log('Uknown target', e.target)
				}
		}

	schema.catch = ( x, y ) =>
		{
			schema.catched.x = x
			schema.catched.y = y
		}

	schema.drag =
		{
			start: e =>
				{
					if (e.target.className.indexOf('workspace') < 0) return

					const ws = angular.element($workspace)[0]

					schema.catch(e.screenX + ws.scrollLeft, e.screenY + ws.scrollTop)
				},
			move: e =>
				{
					if (!schema.catched.x) return

					const ws = angular.element($workspace)[0]

					ws.scrollLeft = -1 * (e.screenX - schema.catched.x)
					ws.scrollTop = -1 * (e.screenY - schema.catched.y)
				},
			end: e => schema.catch()
		}

	//Hidden
	function removeFromBlock ()
		{
			const block = Blocks.find(schema.model._parent, schema.model._block)

			block.schema = null
			block._schema = null

			Schemas.removeFromBlock(schema.model)
			Blocks.save(block)

			Workspace.set.schema(schema.model._parent)
		}

	angular.element($workspace)
		.bind('mouseup', schema.dropHandle)
		.bind('mousedown', schema.drag.start)
		.bind('mousemove', schema.drag.move)

	angular.element(window)
		.bind('mouseup', schema.dropHandle)
		.bind('mousemove', schema.drag.move)
}
