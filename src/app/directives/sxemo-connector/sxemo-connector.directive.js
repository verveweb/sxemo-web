'use strict'

import connectorCtrl from './sxemo-connector.controller.js'
import connectorTmpl from './sxemo-connector.template.html'

import '../../../style/components/sxemo-connector.css'

export default function()
{
	return {
		replace: true,
		restrict: 'E',
		scope:
			{
				model: '=?',
				parent: '=',
				auto: '='
			},
		template: connectorTmpl,
		controller: connectorCtrl,
		controllerAs: 'connector'
	}
}