'use strict'

export default function ( $scope, $attrs, $element, Line, Blocks, Workspace, $timeout, Tools )
{
	const connector = this

	connector.model = $scope.model || ''
	connector.line = new Line($element[0])
	connector.parent = $scope.parent

	connector.active = false
	connector.canStart = ($attrs.active !== undefined)

	$scope.parent._element.connectors.push(connector)

	$scope.$watch('auto', function(){
		if ($scope.auto) connector.dragStart()
	})

	connector.dragStart = function ( e )
		{
			if (!connector.line.target)
			{
				Workspace.set.block($scope.parent)
				connector.model = ''

				connector.active = true
				
				connector.line.start(e)
				connector.line.update(e)
			}
		}

	connector.update = function ( e )
		{
			if (!e) return
			if (!e.target) return
			if (e.buttons < 1) return

			if (!connector.active)
			{
				if (!Tools.has.parent(e.target,'sxemo-block')) return

				if (!(
					$scope.parent._element._element.contains(e.target) || 
					e.target === $scope.parent._element
				)) return
			}

			connect(connector)

			if (connector.active && e.shiftKey)
			{
				let hit = Tools.get.parent(e.target, 'sxemo-block')

				if (hit !== $scope.parent._element._element)
				{
					let blockB = Workspace.findBlock(hit.id)
					Blocks.connect($scope.parent, blockB)
					Workspace.set.block(blockB)
					connector.line.end(e)

					connector.active = false
				}
			}

			if (connector.active || connector.line.target)
			{
				$scope.$apply(function(){
					connector.line.update(e)
				})
			}

			$scope.$apply(() => {
				$scope.parent.inserts.map(i => {
					Workspace.findBlock(i)._element.connectors.map(c => {
						connect(c)
						c.line.update()
					})
				})
			})
		}

	connector.dragEnd = function ( e )
		{
			if (connector.active)
			{
				$scope.$apply(function(){
					Workspace.set.block(null)
					connector.line.end(e)

					connector.active = false
				})
				
				$scope.parent._element.resize(0,0)
			}
		}

	function connect ( ctr )
	{
		if (ctr.model !== ''  && !ctr.line.target)
		{
			ctr.line.target = Workspace.findInsert(ctr.parent.uid, ctr.model)
			ctr.line.visible = true
		}
	}

	angular.element(document).bind('mousemove', connector.update)
	angular.element(document).bind('mouseup', connector.dragEnd)

	$timeout(function(){
		connect(connector)
		connector.line.update()
	}, 500)
}