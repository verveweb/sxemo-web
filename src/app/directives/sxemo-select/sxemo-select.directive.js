'use strict'

import selectCtrl from './sxemo-select.controller.js'
import selectTmpl from './sxemo-select.template.html'

import '../../../style/components/sxemo-select.css'

export default function()
{
	return {
		replace: true,
		restrict: 'E',
		scope:
			{
				onSelect: '=',
				onRemove: '=',
				onRestore: '=',
				onChange: '=',
				options: '=',
				model: '='
			},
		template: selectTmpl,
		controller: selectCtrl,
		controllerAs: 'select'
	}
}