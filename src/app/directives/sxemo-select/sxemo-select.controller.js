'use strict'

export default function ( $scope, Tools )
{
	const select = this

	select.options = $scope.options
	select.model = $scope.model

	$scope.$watch('model', function(){
		select.model = $scope.model
	})

	select.open = false

	select.change = function ()
		{
			;($scope.onChange) && ($scope.onChange(select.model))
		}

	select.select = function ( item )
		{
			if (item._removed) return 
			$scope.onSelect(item)
			select.model = item
			select.open = false
		}

	select.remove = function ( item )
		{
			$scope.onRemove(item)
		}

	select.restore = function ( item )
		{
			$scope.onRestore(item)
		}

	select.canRemove = function ()
		{
			if (select.options.length < 2) return false

			var res = select.options.length

			for (let i = 0; i < select.options.length; i++)
			{
				;(select.options[i]._removed) && (res--)
			}

			return (res > 1)
		}

	angular.element(document).bind('click', function ( e ) {
		if (!Tools.has.parent(e.target, 'sxemo-select'))
		{
			$scope.$apply(function(){
				select.open = false
			})
		}
	})
}