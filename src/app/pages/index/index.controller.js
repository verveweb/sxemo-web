'use strict'

export default function ( $scope, $timeout, Schemas, Workspace, Status, Settings )
{
	//Vars
	const vm = this
	let startSchema = Settings.get('schema')

	//Props
	vm.schema = null

	//Watchers
	Workspace.watch('schema', () => {
		vm.schema = Workspace.get.schema()
	})

	//Timers
	$timeout(() => {
		Status.set('Welcome to Sxemo!')
	}, 2000)

	//VOID MAIN
	startSchema = (startSchema === null) 
		? Schemas.get() 
		: Schemas.find(startSchema)

	Workspace.set.schema(startSchema)
}