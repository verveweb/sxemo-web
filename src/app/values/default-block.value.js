'use strict'

export default function ()
{
	this.uid = ''
	this.title = 'Block'
	this.content = ''

	this.inserts = []
	this.returns = []
	this.schema = null

	this.height = 55
	this.position =
		{
			left: 20,
			top: 20
		}

	this.locked = false
	this.color = null

	this._new = true
	this._schema = null
}