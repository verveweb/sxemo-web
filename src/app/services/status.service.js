'use strict'

export default function ()
{
	const status = this,
		observers = []

	let _text = ''

	status.set = function ( text )
		{
			_text = text
			notify()
		}

	status.watch = function ( callback )
		{
			observers.push(callback)
		}

	function notify ()
		{
			observers.map(function ( callback ){
				callback(_text)
			})
		}
}