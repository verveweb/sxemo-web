'use strict'

export default function ( SchemasFactory, DB )
{
	const schemas = this

	schemas.removeFromBlock = function ( sch )
		{
			DB.forget.schema(sch)
		}

	schemas.get = function ( index )
		{
			index = index || schemas.findNotRemovedIndex()
			return schemas.list[index]
		}

	schemas.find = function ( uid )
		{
			var res = schemas.list.filter(function ( s ){
				if (s.uid === uid) return s
			})

			res = (res.length < 1)
				? DB.load.schema(uid)
				: res[0]

			return res
		}

	schemas.add = function ()
		{
			schemas.list.push(SchemasFactory.create())
			return schemas.list[schemas.list.length - 1]
		}

	schemas.export = () =>
		{
			var catchSchema = sid =>
				{
					var scheme = JSON.parse( window.localStorage.getItem( sid ) )

					scheme.blocks = scheme.blocks
						.map( bid => JSON.parse( window.localStorage.getItem( bid ) ) )
						.map( block => {
							delete block.returns
							;( block.schema ) && ( block.schema = catchSchema( block.schema ) )
							return block
						})

					return scheme
				},

				schemas = JSON.parse( window.localStorage.getItem( 'sxemoSchemas' ) ).map( catchSchema ),
				a = document.createElement('a')

				a.setAttribute('href', 'data:application/json;charset=utf-8,' + encodeURIComponent( JSON.stringify( schemas ) ) )
				a.setAttribute('download', 'sxemo-export.json')

				document.body.appendChild( a )

				a.click()

				document.body.removeChild( a )
		}

	schemas.findNotRemovedIndex = function ()
		{
			for (let i = 0; i < schemas.list.length; i++)
			{
				if (!schemas.list[i]._removed) return i
			}

			return 0
		}

	schemas.load = function ()
		{
			var schs = DB.load.schemas()

			;(schs === null) && (schs = [SchemasFactory.create()], schemas.save(schs))

			return schs
		}

	schemas.save = function ( schs )
		{
			schs = schs || schemas.list
			DB.save.schemas(schs)
		}

	schemas.saveSchema = function ( sch )
		{
			DB.save.schema(sch)
		}

	schemas.list = schemas.load()

	return schemas
}
