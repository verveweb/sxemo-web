'use strict'

export default function ( Status )
{
	const db = this

	db.save = 
		{
			schemas: function ( list )
				{
					//Save schemas list
					save('sxemoSchemas', uids(list))

					//Save schemas
					list.map(function ( l ) {
						db.save.schema(l, true)
					})

					Status.set('Changes saved')
				},
			schema: function ( sch, silent )
				{
					save(sch.uid, clear(sch, true))

					sch.blocks.map(function ( b ){
						db.save.block(b, true)
					})

					;(!silent) && (Status.set('Changes saved'))
				},
			block: function ( block, silent )
				{
					save(block.uid, clear(block))
					;(block.schema !== null) && (db.save.schema(block._schema, true))

					;(!silent) && (Status.set('Changes saved'))
				}
		}

	db.load =
		{
			schemas: function ()
				{
					var suids = load('sxemoSchemas')

					return (!suids) 
						? null 
						: suids.map(function ( uid ){
									return db.load.schema(uid)
								})
								.filter(function ( sch ){
									return sch
								})
				},
			schema: function ( uid )
				{
					var tmp = load(uid)

					if (tmp === null) return

					tmp.blocks = tmp.blocks.map(function ( buid ){
						return db.load.block(buid, tmp)
					})

					return tmp
				},
			block: function ( uid, parent )
				{
					var tmp = load(uid)

					tmp._parent = parent

					if (tmp.schema !== null)
					{
						tmp._schema = db.load.schema(tmp.schema)
						tmp.schema = tmp._schema.uid
						tmp._schema._block = uid
					}
					else
					{
						tmp._schema = null
					}

					return tmp
				}
		}

	db.forget =
		{
			block: function ( block )
				{
					;(block.schema !== null) && (db.forget.schema(block._schema))
					forget(block.uid)
				},
			schema: function ( schema )
				{
					schema.blocks.map(function(b){
						db.forget.block(b)
					})
					forget(schema.uid)
				}
		}

	function forget ( name )
		{
			window.localStorage.removeItem(name)
		}

	function save ( name, data )
		{
			window.localStorage.setItem(name, JSON.stringify(data))
		}

	function load ( name )
		{
			var tmp = window.localStorage.getItem(name)

			if (tmp === null) return null

			if (JSON.parse(tmp)._removed) 
			{
				tmp = JSON.parse(tmp)

				if (tmp.blocks)
				{
					tmp.blocks.map(function(b){
						forget(b)
					})
				}

				forget(name)
				return null
			}

			return JSON.parse(tmp)
		}

	function clear ( data, arrays )
		{
			var tmp = {}

			for (let key in data)
			{
				;((key[0] !== '$' && key[0] !== '_') || key === '_removed') && (tmp[key] = data[key])
				;(data[key] instanceof Array && arrays) && (tmp[key] = uids(data[key]))
			}

			return tmp
		}

	function uids ( list )
		{
			return list.map(function ( l ) { return l.uid })
		}
}