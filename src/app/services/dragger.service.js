'use strict'

export default function ()
{
	return function ( handler )
	{
		const dragger = this

		dragger.lock = false
		dragger.node = null

		dragger.start = function ( e )
			{
				dragger.node = e.target

				while (dragger.node.className.indexOf('draggable') < 0)
				{
					dragger.node = dragger.node.parentNode
				}

				dragger.node.className += ' draggin'

				dragger.oX = e.clientX - parseInt(dragger.node.parentNode.style.left)
				dragger.oY = e.clientY - parseInt(dragger.node.parentNode.style.top)

				dragger.lock = true
			}

		dragger.end = function ( e )
			{
				if (dragger.lock)
				{
					dragger.lock = false
					dragger.node.className = dragger.node.className.replace(/ draggin/g, '')
				}
			}

		dragger.move = function ( e )
			{
				if (dragger.lock)
				{
					handler(Math.max(0, e.pageX - dragger.oX), Math.max(0, e.pageY - dragger.oY), e)
				}
			}

		angular.element(document).bind('mousemove', function ( e ){
			dragger.move(e)
		})

		angular.element(document).bind('mouseup', function ( e ){
			dragger.end(e)
		})
	}
}