'use strict'

export default function ()
{
	const settings = this,
		config =
			{
				schema: null
			}

	let stored = window.localStorage.getItem('sxemoSettings')

	if (stored)
	{
		stored = JSON.parse(stored)
		config.schema = stored.schema
	}

	settings.save = function ()
		{
			window.localStorage.setItem('sxemoSettings', JSON.stringify(config))
		}

	settings.get = function ( key )
		{
			return config[key]
		}

	settings.set = function ( key, value )
		{
			config[key] = value
			settings.save()
		}

	return settings
}