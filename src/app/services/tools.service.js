'use strict'

export default function ()
{
	const tools = this

	tools.offset = {}

	tools.offset.find = function ( el, what )
		{
			var which = ['offset' + what],
				off = 0

			while (el !== document && el !== null)
			{
				off += parseInt(el[which]) - el['scroll' + what]
				el = el.parentNode
			}

			return off
		}

	tools.offset.left = function ( el )
		{
			return tools.offset.find(el, 'Left')
		}

	tools.offset.top = function ( el )
		{
			return tools.offset.find(el, 'Top')
		}

	tools.guid = function () 
		{
			function s4 () 
				{
					return Math.floor((1 + Math.random()) * 0x10000)
						.toString(16)
						.substring(1)
				}

			return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()
		}

	tools.has = 
		{
			parent: function ( node, cls )
				{
					let res = false

					while (node !== document && node !== null && !res)
					{
						if(typeof node.className !== 'string') return false
						res = res || node.className.indexOf(cls) > -1
						node = node.parentNode
					}

					return res
				}
		}

	tools.get =
		{
			parent: function ( node, cls )
				{
					while (node !== document && node !== null)
					{
						if ([].slice.call(node.classList).indexOf(cls) > -1) return node
						node = node.parentNode
					}

					return node
				}
		}
}