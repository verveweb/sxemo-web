'use strict'

export default function ( Settings )
{
	const ws = this,
		observers = 
			{
				block: [],
				schema: []
			}

	let curBlock = null,
		curSchema = null

	ws.set = 
		{
			block: function ( block )
				{
					curBlock = block
					notify('block')
				},
			schema: function ( schema )
				{
					curSchema = schema
					Settings.set('schema', schema.uid)
					notify('schema')
				}
		}

	ws.get =
		{
			block: function () { return curBlock },
			schema: function () { return curSchema }
		}

  	ws.watch = function ( what, callback )
  		{
  			observers[what].push(callback)
  		}

	ws.findInsert = function ( uid, target )
		{
			if (!ws.findBlock(target)) return
				
			var index = ws.findBlock(target).inserts.indexOf(uid),
				inserts = document.getElementById(target)
					.querySelectorAll('.sxemo-block-inserts .sxemo-connector')

			return inserts[index]
		}

	ws.findReturn = function ( uid, target )
		{
			if (!ws.findBlock(target)) return

			var index = ws.findBlock(target).returns.indexOf(uid),
				returns = document.getElementById(target)
					.querySelectorAll('.sxemo-block-returns .sxemo-connector')

			return returns[index]
		}

	ws.findBlock = function ( uid )
		{
			for (let i = 0; i < curSchema.blocks.length; i++)
			{
				if (curSchema.blocks[i].uid === uid) return curSchema.blocks[i]
			}

			return null
		}

  	function notify ( what )
  		{
  			angular.forEach(observers[what], function ( callback ){
  				callback()
  			})
  		}
}