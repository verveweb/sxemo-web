'use strict'

import dlgExampleTmpl from '../dialogs/example.template.html'
import dlgExampleCtrl from '../dialogs/example.controller.js'

//Remove schema dialog
import dlgRemoveSchemaTmpl from '../dialogs/remove-schema/remove-schema.template.html'
import dlgRemoveSchemaCtrl from '../dialogs/remove-schema/remove-schema.controller.js'

//Help dialog
import dlgHelpTmpl from '../dialogs/help/help.template.html'
import dlgHelpCtrl from '../dialogs/help/help.controller.js'

//Settings dialog
import dlgSettingsTmpl from '../dialogs/settings/settings.template.html'
import dlgSettingsCtrl from '../dialogs/settings/settings.controller.js'

import '../../style/ngDialog.min.css'
import '../../style/ngDialog-theme-default.min.css'
import '../../style/dialogs.css'

export default function ( $rootScope, ngDialog )
{
	const dialogs = this

	dialogs.example = function ( data, onClose )
		{
			_dialog(dlgExampleTmpl, dlgExampleCtrl, { title: 'Hello', data: data }, onClose)
		}

	dialogs.removeSchema = function ( add, onClose )
		{
			_dialog(dlgRemoveSchemaTmpl, dlgRemoveSchemaCtrl, { add: add }, onClose)
		}

	dialogs.help = function ()
		{
			_dialog(dlgHelpTmpl, dlgHelpCtrl, { wide: true })
		}

	dialogs.settings = function ( config, onClose )
		{
			_dialog(dlgSettingsTmpl, dlgSettingsCtrl, config, onClose)
		}

	function _dialog ( tmpl, ctrl, data, callback )
		{
			let $sc = $rootScope.$new()

			angular.forEach(data, function(item, key){
				$sc[key] = item
			})

			ngDialog.open({ 
				template: tmpl, 
				controller: ctrl, 
				controllerAs: 'dlg', 
				plain: true, 
				scope: $sc,
				className: (data && data.wide) ? 'ngdialog-theme-default wide' : 'ngdialog-theme-default',
				preCloseCallback: function ( value ) 
					{
						;(callback) && (callback(value))
				    } 
			});
		}

	return dialogs
}