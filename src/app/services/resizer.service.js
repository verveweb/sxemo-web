'use strict'

export default function ()
{
	return function ( handler )
	{
		const resizer = this

		resizer.lock = false
		resizer.node = null

		resizer.start = function ( e )
			{
				resizer.node = e.target

				while (resizer.node.className.indexOf('resizable') < 0)
				{
					resizer.node = resizer.node.parentNode
				}

				resizer.node.className += ' resizing'

				resizer.oX = e.pageX - resizer.node.offsetWidth
				resizer.oY = e.pageY - resizer.node.offsetHeight

				resizer.lock = true
			}

		resizer.end = function ( e )
			{
				if (resizer.lock)
				{
					resizer.lock = false
					resizer.node.className = resizer.node.className.replace(/ resizing/g, '')
				}
			}

		resizer.move = function ( e )
			{
				if (resizer.lock)
				{
					handler(Math.max(0, e.pageX - resizer.oX), Math.max(0, e.pageY - resizer.oY))
				}
			}

		angular.element(document).bind('mousemove', function ( e ){
			resizer.move(e)
		})

		angular.element(document).bind('mouseup', function ( e ){
			resizer.end(e)
		})
	}
}