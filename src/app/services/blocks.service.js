'use strict'

export default function ( BlocksFactory, Workspace, DB )
{
	const blocks = this

	blocks.find = function ( sch, uid )
		{
			var tmp = sch.blocks.filter(function(b){
				if (b.uid === uid) return b
			})

			return (tmp.length > 0) ? tmp[0] : null
		}

	blocks.save = function ( block )
		{
			DB.save.block(block)
		}

	blocks.remove = function ( block )
		{
			blocks.clear(block)
			block._parent.blocks.splice(block._parent.blocks.indexOf(block), 1)
			DB.save.schema(block._parent)
			DB.forget.block(block)
		}

	blocks.clear = function ( block, what )
		{
			if (!what)
			{
				blocks.clear(block, 'inserts')
				what = 'returns'
			}

			var cl = block[what].map(function ( e ) {
				return [Workspace.findBlock(e), block]
			})

			cl.map(function(e){
				;(what === 'returns')
					? blocks.disconnect(e[1], e[0])
					: blocks.disconnect(e[0],e[1])
			})
		}

	blocks.connect = function ( blockA, blockB )
		{
			if (blockA.uid === blockB.uid) return
				
			if (blockA.returns.indexOf(blockB.uid) < 0)
			{
				blockA.returns.push(blockB.uid)
				blockB.inserts.push(blockA.uid)

				blocks.save(blockA)
				blocks.save(blockB)
			}
		}

	blocks.disconnect = function ( blockA, blockB )
		{
			blockA.returns.splice(blockA.returns.indexOf(blockB.uid),1)
			blockB.inserts.splice(blockB.inserts.indexOf(blockA.uid),1)

			blocks.save(blockA)
			blocks.save(blockB)
		}

	return blocks
}