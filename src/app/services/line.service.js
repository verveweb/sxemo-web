'use strict'

export default function ( Tools )
{
	return function ( parent )
	{
		const line = this

		line.visible 	= false
		line.target 	= null

		line.box 		= [1, 1]
		line.startPoint = [0, 0]
		line.endPoint 	= [0, 0]
		
		line.start = function ( e )
			{
				line.visible = true
				line.target = null
				line.startPoint = [Tools.offset.left(parent), Tools.offset.top(parent)]
			}

		line.end = function ( e )
			{
				;(!line.target) && (line.visible = false)
			}

		line.path = function ()
			{
				var ex = line.endPoint[0] - line.startPoint[0],
					sx1 = (ex > 0) ? Math.round(ex / 2) : -ex,
					sx2 = (ex > 0) ? sx1 : ex * 2,
					sy1 = (ex > 0) ? 0 : Math.round((line.endPoint[1] - line.startPoint[1]) / 2),
					sy2 = (ex > 0) ? line.endPoint[1] - line.startPoint[1] : Math.round((line.endPoint[1] - line.startPoint[1]) / 2)

				return 'M' + 0 + ',' + 0 + ' C' +
					[
						sx1, sy1, 
						sx2, sy2, 
						ex, line.endPoint[1] - line.startPoint[1]
					].join(',')
			}

		line.fitBox = function ()
			{
				line.box = 
					[
						Math.max(40, line.endPoint[0] - line.startPoint[0]),
						Math.max(40, line.endPoint[1] - line.startPoint[1])
					]
			}

		line.update = function ( e )
			{
				line.startPoint = [Tools.offset.left(parent), Tools.offset.top(parent)]

				if (line.visible)
				{
					if (!line.target)
					{
						if (!e) 
						{
							e = 
								{ 
									pageX: Tools.offset.left(parent) - 150, 
									pageY: Tools.offset.top(parent) - 55
								}
						}

						line.endPoint = 
							[
								e.pageX + parseInt(parent.offsetLeft - parent.clientWidth),
								e.pageY + 40 + parent.clientHeight
							]
					}
					else
					{
						line.endPoint =
							[
								Tools.offset.left(line.target) + parseInt(parent.offsetLeft - parent.clientWidth),
								Tools.offset.top(line.target)
							]
					}

					line.fitBox()
				}
			}
	}
}